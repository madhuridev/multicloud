apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: result
  name: result
  namespace: vote
spec:
  replicas: 1
  selector:
    matchLabels:
      app: result
  template:
    metadata:
      labels:
        app: result
    spec:
      containers:
      - image: registry.gitlab.com/madhuridev/multicloud/result:GIT_COMMIT
        name: result
        ports:
        - containerPort: 80
          name: result
