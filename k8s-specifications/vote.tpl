apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: vote
  name: vote
  namespace: vote
spec:
  replicas: 1
  selector:
    matchLabels:
      app: vote
  template:
    metadata:
      labels:
        app: vote
    spec:
      containers:
      - image: registry.gitlab.com/madhuridev/multicloud/vote:GIT_COMMIT
        name: vote
        ports:
        - containerPort: 80
          name: vote
